# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment   = "data-cluster"
  region       = "us-central1"
  name         = "${var.environment}-${local.deployment}"
  cluster_name = "${var.environment}-data-cluster"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  machines_map = {
    for machine in var.machines : machine.email => merge(machine, {
      member = "user:${machine.email}"
    })
  }
}
