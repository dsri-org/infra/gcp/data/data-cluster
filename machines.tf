# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "google_kms_key_ring" "keyring" {
  name     = "data-cluster-keyring"
  location = local.region
}

resource "google_kms_crypto_key" "data-key" {
  name            = "data-cluster-crypto-key"
  key_ring        = google_kms_key_ring.keyring.id
  rotation_period = "100000s"

  lifecycle {
    prevent_destroy = true
  }
}

# Trivy doesn't like that GCP recommends a broad subnet to allow access to GCP services.
#trivy:ignore:AVD-GCP-0027
resource "google_compute_firewall" "data-firewall" {
  name    = "allow-incoming-ssh-from-iap"
  network = google_compute_network.data.id

  direction = "INGRESS"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = [
    "35.235.240.0/20"
  ]
}

resource "google_iap_tunnel_iam_member" "allow_remote_access_to_iap" {
  member = "group:gcp-compute-users@dsri.org"
  role   = "roles/iap.tunnelResourceAccessor"
}

resource "google_project_iam_member" "use_tunnel" {
  member  = "group:gcp-compute-users@dsri.org"
  project = var.project_id
  role    = "roles/iap.tunnelResourceAccessor"
}

resource "google_project_iam_member" "os_login" {
  member  = "group:gcp-compute-users@dsri.org"
  project = var.project_id
  role    = "roles/compute.osLogin"
}

resource "google_service_account" "machine" {
  for_each = local.machines_map

  account_id   = "tf-${local.deployment}-${each.value.machine_name}"
  display_name = "tf-${local.deployment}-${each.value.machine_name}"
}

resource "google_service_account_iam_member" "machine_user" {
  for_each           = local.machines_map
  service_account_id = google_service_account.machine[each.key].name
  role               = "roles/iam.serviceAccountUser"
  member             = each.value.member
}

# container.clusterViewer provides minimal cluster access
# https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#define-and-assign
# resource "google_project_iam_member" "machine_user_cluster_viewer" {
#   for_each = local.machines_map
#   member   = each.value.member
#   project  = var.project
#   role     = "roles/container.clusterViewer"
# }

resource "google_service_account_iam_member" "machine_admin" {
  for_each           = local.machines_map
  service_account_id = google_service_account.machine[each.key].name
  role               = "roles/iam.serviceAccountUser"
  member             = "group:gcp-compute-admins@dsri.org"
}

resource "google_compute_instance" "machine" {
  for_each     = local.machines_map
  name         = each.value.machine_name
  machine_type = each.value.machine_type
  zone         = "us-central1-a"

  boot_disk {
    kms_key_self_link = google_kms_crypto_key.data-key.id
    initialize_params {
      image = data.google_compute_image.ubuntu.self_link
    }
  }

  shielded_instance_config {
    enable_vtpm                 = true
    enable_integrity_monitoring = true
    enable_secure_boot          = true
  }

  network_interface {
    subnetwork = google_compute_subnetwork.data.name
  }

  service_account {
    email = google_service_account.machine[each.key].email
    # the cloud-platform scope is required for VM Manager to work
    scopes = ["cloud-platform"]
  }

  metadata = {
    block-project-ssh-keys = true
    enable-oslogin         = true
    enable-osconfig        = true
  }

  allow_stopping_for_update = true

  lifecycle {
    ignore_changes = [
      metadata["ssh-keys"],
    ]
  }
}
