# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "google_kms_key_ring" "data" {
  name     = "tf-${local.deployment}-keyring"
  location = local.region
}

resource "google_kms_crypto_key" "data" {
  name            = "tf-${local.deployment}-key"
  key_ring        = google_kms_key_ring.data.id
  rotation_period = "86400s"

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_service_account" "cluster_node" {
  account_id   = "tf-${local.deployment}-cluster-node"
  display_name = "tf-${local.deployment}-cluster-node"
}

# https://cloud.google.com/kubernetes-engine/docs/how-to/service-accounts#default-gke-service-agent
# https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster#use_least_privilege_sa
resource "google_project_iam_member" "cluster_node" {
  for_each = toset([
    "roles/autoscaling.metricsWriter",
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
    "roles/stackdriver.resourceMetadata.writer",
  ])
  project = data.google_client_config.current.project
  role    = each.key
  member  = "serviceAccount:${google_service_account.cluster_node.email}"
}

# Serial port logging is definitely required by GKE Autopilot for normal
# operation:
#
# > "If serial port logging is disabled, Autopilot can't provision nodes to run
# > your workloads."
#
# https://cloud.google.com/kubernetes-engine/docs/troubleshooting/autopilot-clusters#scale-up-failed-serial-port-logging
resource "google_compute_project_metadata_item" "enable_serial_port_logging" {
  key   = "serial-port-logging-enable"
  value = "true"
}

# Trivy is failing on settings that are only applicable to non-Autopilot
# clusters. It also fails because we're not using the authorized network feature,
# but doing so would be prohibitively difficult to use with GitLab Shared
# Runners.
#trivy:ignore:AVD-GCP-0048
#trivy:ignore:AVD-GCP-0050
#trivy:ignore:AVD-GCP-0061
resource "google_container_cluster" "data" {
  name     = local.name
  location = google_compute_router.data.region
  project  = var.project_id

  network    = google_compute_network.data.id
  subnetwork = google_compute_subnetwork.data.id

  deletion_protection = false
  enable_autopilot    = true

  addons_config {
    gke_backup_agent_config {
      enabled = true
    }
  }

  authenticator_groups_config {
    security_group = "gke-security-groups@dsri.org"
  }

  cluster_autoscaling {
    auto_provisioning_defaults {
      management {
        auto_upgrade = true
        auto_repair  = true
      }

      service_account = google_service_account.cluster_node.email
    }
  }

  cost_management_config {
    enabled = true
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "pod-ranges"
    services_secondary_range_name = "services-range"
  }

  logging_config {
    enable_components = [
      "SYSTEM_COMPONENTS",
      "WORKLOADS",
    ]
  }

  monitoring_config {
    enable_components = [
      "SYSTEM_COMPONENTS",
      "APISERVER",
      "SCHEDULER",
      "CONTROLLER_MANAGER",
      "STORAGE",
      "HPA",
      "DAEMONSET",
      "DEPLOYMENT",
      "STATEFULSET",
    ]

    managed_prometheus {
      enabled = true
    }
  }

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
  }

  release_channel {
    channel = "STABLE"
  }

  resource_labels = local.default_tags

  service_external_ips_config {
    enabled = false
  }

  # lifecycle {
  #   prevent_destroy = true
  # }
}
