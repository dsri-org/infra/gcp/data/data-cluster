# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "google_cloud_service_account_file" {
  type = string
}

variable "environment" {
  type = string
}

variable "project_id" {
  type = string
}

variable "project_number" {
  type = string
}

variable "postgresql_disk_storage" {
  type = string
}

variable "postgresql_database_tier" {
  type = string
}

variable "argo_postgresql_database_tier" {
  type = string
}

variable "argo_postgresql_disk_storage" {
  type = string
}

variable "lens_postgresql_database_tier" {
  type = string
}

variable "lens_postgresql_disk_storage" {
  type = string
}

variable "machines" {
  type    = list(object({ email = string, machine_name = string, machine_type = string }))
  default = []
}
