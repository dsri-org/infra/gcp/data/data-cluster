# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# create the vpc
resource "google_compute_network" "data" {
  name                            = local.name
  delete_default_routes_on_create = false
  auto_create_subnetworks         = false
  routing_mode                    = "REGIONAL"
}

# create private subnet and assign addresses to all the zones in the region
resource "google_compute_subnetwork" "data" {
  name          = "${local.name}-subnetwork"
  ip_cidr_range = "10.0.0.0/16"
  region        = local.region
  network       = google_compute_network.data.id

  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "pod-ranges"
    ip_cidr_range = "192.168.0.0/18"
  }

  secondary_ip_range {
    range_name    = "services-range"
    ip_cidr_range = "192.168.64.0/18"
  }

  log_config {
    aggregation_interval = "INTERVAL_10_MIN"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_router" "data" {
  name    = "${local.name}-router"
  region  = google_compute_subnetwork.data.region
  network = google_compute_network.data.id
}

resource "google_compute_router_nat" "data" {
  name                               = "${local.name}-router-nat"
  router                             = google_compute_router.data.name
  region                             = google_compute_router.data.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = "${local.name}-subnetwork"
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

# IP to route the
resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.data.id
}

# connecting the database to the VPC
resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.data.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}
