# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "project_number" {
  value = data.google_project.data.number
}

output "project_id" {
  value = data.google_project.data.project_id
}

output "databases" {
  value = {
    "argo"     = module.argo
    "pipeline" = module.pipeline
    "lens"     = module.lens
  }
  sensitive = true
}
