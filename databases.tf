# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "argo" {
  source = "./modules/postgresql"

  name       = "argo"
  project_id = var.project_id

  backup_region            = "us-west1"
  disk_autoresize_limit    = var.argo_postgresql_disk_storage
  private_network          = google_compute_network.data.self_link
  region                   = local.region
  tags                     = local.default_tags
  postgresql_database_tier = var.argo_postgresql_database_tier

  databases = ["argo"]
  users     = ["argo"]

  cluster_service_accounts = [
    "adminer/adminer",
    "argo/argo-argo-workflows-server",
    "argo/argo-argo-workflows-workflow-controller",
  ]

  depends_on = [google_service_networking_connection.private_vpc_connection]
}

module "pipeline" {
  source = "./modules/postgresql"

  name       = "pipeline"
  project_id = var.project_id

  backup_region            = "us-west1"
  disk_autoresize_limit    = var.postgresql_disk_storage
  private_network          = google_compute_network.data.self_link
  region                   = local.region
  tags                     = local.default_tags
  postgresql_database_tier = var.postgresql_database_tier

  databases = ["projects"]
  users     = ["projects"]

  cluster_service_accounts = [
    "adminer/adminer",
    "pipeline-database/pipeline-database",
    "evaluations/evaluations-login",
  ]

  depends_on = [google_service_networking_connection.private_vpc_connection]
}

module "lens" {
  source = "./modules/postgresql"

  name       = "lens"
  project_id = var.project_id

  backup_region            = "us-west1"
  disk_autoresize_limit    = var.lens_postgresql_disk_storage
  private_network          = google_compute_network.data.self_link
  region                   = local.region
  tags                     = local.default_tags
  postgresql_database_tier = var.lens_postgresql_database_tier

  databases = ["lens"]
  users     = ["lens"]

  cluster_service_accounts = [
    "adminer/adminer",
    "lens/lens",
  ]

  depends_on = [google_service_networking_connection.private_vpc_connection]
}
