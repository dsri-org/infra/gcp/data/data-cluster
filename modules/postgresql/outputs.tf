# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "name" {
  value = var.name
}

output "instance" {
  value = {
    self_link          = google_sql_database_instance.this.self_link
    connection_name    = google_sql_database_instance.this.connection_name
    private_ip_address = google_sql_database_instance.this.private_ip_address
    host               = google_sql_database_instance.this.private_ip_address
    port               = 5432
  }
}

output "databases" {
  value = toset(var.databases)
}

output "users" {
  value = merge({
    "postgres" = google_sql_database_instance.this.root_password
    }, {
    for user in var.users : google_sql_user.user[user].name => google_sql_user.user[user].password
  })
  sensitive = true
}
