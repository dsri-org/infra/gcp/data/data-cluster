# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
resource "random_password" "root_password" {
  length  = 32
  special = false
}

resource "random_string" "suffix" {
  length  = 6
  special = false
  upper   = false
}

resource "google_sql_database_instance" "this" {
  name                = "${var.name}-${random_string.suffix.id}"
  region              = var.region
  database_version    = "POSTGRES_15"
  deletion_protection = false
  root_password       = random_password.root_password.result

  settings {
    backup_configuration {
      enabled                        = true
      location                       = var.backup_region
      point_in_time_recovery_enabled = true
    }

    connector_enforcement = "REQUIRED"

    database_flags {
      name  = "log_temp_files"
      value = "0"
    }

    database_flags {
      name  = "log_connections"
      value = "on"
    }

    database_flags {
      name  = "log_lock_waits"
      value = "on"
    }

    database_flags {
      name  = "log_disconnections"
      value = "on"
    }

    database_flags {
      name  = "log_checkpoints"
      value = "on"
    }

    disk_autoresize       = true
    disk_autoresize_limit = var.disk_autoresize_limit

    ip_configuration {
      enable_private_path_for_google_cloud_services = true
      ipv4_enabled                                  = false
      private_network                               = var.private_network
      require_ssl                                   = true
      ssl_mode                                      = "TRUSTED_CLIENT_CERTIFICATE_REQUIRED"
    }

    tier = var.postgresql_database_tier

    user_labels = var.tags
  }
}

resource "google_sql_database" "user" {
  for_each = toset(var.databases)
  name     = each.key
  instance = google_sql_database_instance.this.name
}

resource "random_password" "user" {
  for_each = toset(var.users)
  length   = 32
  special  = false
}

resource "google_sql_user" "user" {
  for_each = toset(var.users)
  name     = each.key
  instance = google_sql_database_instance.this.name
  password = random_password.user[each.key].result
}

resource "google_project_iam_member" "user" {
  for_each = toset(var.cluster_service_accounts)
  project  = var.project_id
  role     = "roles/cloudsql.client"
  member   = "serviceAccount:${var.project_id}.svc.id.goog[${each.key}]"
}
