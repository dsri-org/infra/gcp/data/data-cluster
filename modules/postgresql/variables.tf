# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "name" {
  type = string
}

variable "project_id" {
  type = string
}

variable "region" {
  type = string
}

variable "backup_region" {
  type = string
}

variable "disk_autoresize_limit" {
  type    = number
  default = 100
}

variable "private_network" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "databases" {
  type    = list(string)
  default = []
}

variable "users" {
  type    = list(string)
  default = []
}

variable "cluster_service_accounts" {
  type    = list(string)
  default = []
}

variable "postgresql_database_tier" {
  type = string
}
