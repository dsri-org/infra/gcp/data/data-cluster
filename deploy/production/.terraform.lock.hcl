# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/google" {
  version     = "5.18.0"
  constraints = "~> 5.18.0"
  hashes = [
    "h1:rwT/Nbyt86WhxJMS24UhECIfe2oYAMEO+MaXk0Uf9ak=",
    "zh:02e755030e5ea87eefda0f8bb3e6b53df2d2578144c92b4daa21d42336386b83",
    "zh:25c3eb7ec7654f4955abcc700d91e9bc7f0ef196508702194351a97dabc087b9",
    "zh:6dc9014d91bcbb21546e561fe465aed9c8a69b4d465414f8723026b4b47abaea",
    "zh:876fcb3c32a72b21e61fcfac34cc6749edb7247628165eda2e2a3f0282f8e6a0",
    "zh:ac6d119350be3a4463cff3f7b6c7d86abeaee279d01c9c344571eff7d945180a",
    "zh:c28036b74c81007770fe97aae594e2d797eeff51a4807bb37be3891df3109194",
    "zh:d06bac750620247344ea2d57196bdd8e1daed68fe0005a028d059e89116dfe31",
    "zh:d4dbeddfe7bb80e0df8d26ff706d8c897e3e82698f3402b2659050704c0789e0",
    "zh:d604b24b5001da7743f4ce5036e4ef17f7cc54478f7cb327144cef3760e05bf3",
    "zh:e1b8a575928c5bbaa7afa7267474a1bc52da4e9ac526343dfbcc3ef95584fe61",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:tW+G7lgqbHUtraKHPWuotYHlME1vcAf50YvOeHQlGHg=",
    "zh:0002dd4c79453da5bf1bb9c52172a25d042a571f6df131b7c9ced3d1f8f3eb44",
    "zh:49b0f8c2bd5632799aa6113e0e46acaa7d008f927665a41a1f8e8559fe6d8165",
    "zh:56df70fca236caa06d0e636c41ab71dd1ced05375f4ddcb905b0ed2105737048",
    "zh:58e4de40540c86b9e2e2595dac1318ba057718961a467fa9727866f747693eb2",
    "zh:5992f11c738812ccd7476d4c607cb8b76dea5aa612be491150c89957ec395ddd",
    "zh:7ff4f0b7707b51737f684e96d85a47f0dd8be0f72a3c27b0798755d3faad15e2",
    "zh:8e4b0972e216c9773ab525accfa36eb27c44c751b06b125ecc53f4226c91cea8",
    "zh:d8956cc5abcd5d1173b6cc25d5d8ed2c5cc456edab2fddb774a17d45e84820cb",
    "zh:df7f9eb93a832e66bc20cc41c57d38954f87671ec60be09fa866273adb8d9353",
    "zh:eb583d8f03b11f0b6c535375d8ed0d29e5f7f537b5c78943856d2e8ce76482d9",
  ]
}
