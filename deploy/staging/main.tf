# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source                            = "../.."
  google_cloud_service_account_file = var.google_cloud_service_account_file
  environment                       = "staging"
  project_id                        = "staging-data-467236"
  project_number                    = "162201344069"
  postgresql_disk_storage           = "100"
  postgresql_database_tier          = "db-f1-micro"
  argo_postgresql_disk_storage      = "100"
  argo_postgresql_database_tier     = "db-f1-micro"
  lens_postgresql_disk_storage      = "100"
  lens_postgresql_database_tier     = "db-f1-micro"

  machines = [
    { email = "brett.weir@dsri.org", machine_name = "brett", machine_type = "e2-medium" },
    { email = "emily.wright@dsri.org", machine_name = "emily", machine_type = "e2-medium" },
  ]
}
