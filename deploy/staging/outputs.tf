# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "project_number" {
  value = module.root.project_number
}

output "project_id" {
  value = module.root.project_id
}

output "databases" {
  value     = module.root.databases
  sensitive = true
}
