# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

data "google_project" "data" {}

data "google_client_config" "current" {}

data "google_compute_image" "ubuntu" {
  project = "ubuntu-os-cloud"
  # family  = "ubuntu-2404-lts-amd64"
  name = "ubuntu-2404-noble-amd64-v20240626"
}

data "google_storage_project_service_account" "this" {}

data "google_container_cluster" "data" {
  name     = local.cluster_name
  location = local.region
}
