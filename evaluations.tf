# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  evaluations_group_email = "gke-data-evaluations@dsri.org"
}

resource "kubernetes_namespace" "evaluations" {
  metadata {
    name = "evaluations"
  }
}

resource "kubernetes_service_account" "evaluations" {
  metadata {
    name      = "evaluations"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }
  automount_service_account_token = false
}

resource "google_storage_bucket_iam_member" "evaluations" {
  bucket = google_storage_bucket.evaluations_bucket.name
  role   = "roles/storage.objectUser"
  member = "serviceAccount:${var.project_id}.svc.id.goog[evaluations/evaluations]"
}

resource "google_project_iam_member" "evaluations_add_role" {
  project = var.project_id
  role    = google_project_iam_custom_role.evaluations.id
  member  = "group:${local.evaluations_group_email}"
}

resource "google_project_iam_member" "evaluations_viewer" {
  project = var.project_id
  role    = "roles/viewer"
  member  = "group:${local.evaluations_group_email}"
}

resource "google_project_iam_custom_role" "evaluations" {
  role_id = "evaluationsNamespaceUser"
  title   = "Evaluations Namespace User"
  permissions = [
    "container.clusters.connect",
    "container.clusters.get",
    "container.clusters.list",
  ]
}

resource "kubernetes_role" "evaluations" {
  metadata {
    name      = "evaluations"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }
  rule {
    api_groups = [""]
    resources  = ["pods", "pods/portforward", "pods/log", "pods/attach", "pods/exec"]
    verbs      = ["*"]
  }
  rule {
    api_groups = ["apps"]
    resources  = ["deployments", "replicasets"]
    verbs      = ["*"]
  }
  rule {
    api_groups = ["batch"]
    resources  = ["jobs", "cronjobs"]
    verbs      = ["*"]
  }
}

resource "kubernetes_role_binding" "evaluations" {
  metadata {
    name      = "evaluations"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.evaluations.metadata.0.name
  }
  subject {
    kind = "Group"
    name = local.evaluations_group_email
  }
}

resource "kubernetes_cluster_role" "evaluations_namespace" {
  metadata {
    name = "evaluations-namespace-reader"
  }
  rule {
    api_groups = [""]
    resources  = ["namespaces"]
    verbs      = ["get", "list"]
  }
}

resource "kubernetes_cluster_role_binding" "evaluations_namespace" {
  metadata {
    name = "evaluations-namespace-reader-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.evaluations_namespace.metadata.0.name
  }
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Group"
    name      = local.evaluations_group_email
  }
}

# Create the evaluations gcs bucket

resource "random_string" "evaluations_bucket" {
  length  = 8
  special = false
  upper   = false
}

resource "google_kms_key_ring" "evaluations_bucket" {
  name     = "${local.name}-keyring"
  location = local.region
}

resource "google_kms_crypto_key" "evaluations_bucket" {
  name            = "${local.name}-${local.region}"
  key_ring        = google_kms_key_ring.evaluations_bucket.id
  rotation_period = "7776000s"
}

resource "google_kms_crypto_key_iam_binding" "evaluations_bucket" {
  crypto_key_id = google_kms_crypto_key.evaluations_bucket.id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

  members = [
    "serviceAccount:${data.google_storage_project_service_account.this.email_address}",
  ]
}

resource "google_service_account" "evaluations_bucket" {
  account_id                   = google_storage_bucket.evaluations_bucket.name
  display_name                 = google_storage_bucket.evaluations_bucket.name
  create_ignore_already_exists = true
}


resource "google_storage_hmac_key" "evaluations_bucket" {
  service_account_email = google_service_account.evaluations_bucket.email
}

resource "google_storage_bucket" "evaluations_bucket" {
  name          = "evaluations-${random_string.evaluations_bucket.result}"
  location      = local.region
  labels        = local.default_tags
  force_destroy = false # set to true when working

  uniform_bucket_level_access = true
  public_access_prevention    = "enforced"

  encryption {
    default_kms_key_name = google_kms_crypto_key.evaluations_bucket.id
  }

  lifecycle_rule {
    condition {
      age = 30
    }
    action {
      type = "Delete"
    }
  }

  retention_policy {
    is_locked        = false
    retention_period = 2592000 # 30 days
  }

  depends_on = [google_kms_crypto_key_iam_binding.evaluations_bucket]
}

resource "kubernetes_network_policy" "evaluations_internet_only" {
  metadata {
    name      = "evaluations-internet-only"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }

  spec {
    pod_selector {}

    # ingress {

    #   from {
    #     namespace_selector {
    #       match_labels = {
    #         name = "evaluations"
    #       }
    #     }
    #   }
    # }

    egress {
      to {
        ip_block {
          cidr = "0.0.0.0/0"
          except = [
            google_compute_subnetwork.data.ip_cidr_range,
            google_compute_subnetwork.data.secondary_ip_range.0.ip_cidr_range,
            google_compute_subnetwork.data.secondary_ip_range.1.ip_cidr_range,
          ]
        }
      }
    }

    policy_types = [
      #"Ingress",
      "Egress",
    ]
  }
}

resource "kubernetes_network_policy" "evaluations_deny_all" {
  metadata {
    name      = "evaluations-deny-all"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }

  spec {
    pod_selector {}

    policy_types = [
      "Ingress",
      "Egress",
    ]
  }
}
